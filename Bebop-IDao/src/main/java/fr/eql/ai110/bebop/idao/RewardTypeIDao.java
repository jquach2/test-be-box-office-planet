package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.RewardType;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface RewardTypeIDao extends GenericIDao<RewardType> {

	/** returns all rewardTypes that contain these characters in name */
	List<RewardType> getRewardTypesByName(String name);
	
	/** returns the total count of rewardTypes */
	Long countRewardTypes();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
