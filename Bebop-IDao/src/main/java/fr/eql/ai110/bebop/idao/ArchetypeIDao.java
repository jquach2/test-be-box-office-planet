package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.ArchetypeEntity;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface ArchetypeIDao extends GenericIDao<ArchetypeEntity> {

	/** returns all archetype entities that contain these characters in name */
	List<ArchetypeEntity> getArchetypeEntitiesByName(String name);
	
	/** returns the total count of archetype entities */
	Long countArchetypeEntities();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
