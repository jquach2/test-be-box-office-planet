package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Space;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface SpaceIDao extends GenericIDao<Space> {

	/** returns all spaces that contain these characters in name */
	List<Space> getSpacesByName(String name);
	
	/** returns the total count of space */
	Long countSpaces();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
