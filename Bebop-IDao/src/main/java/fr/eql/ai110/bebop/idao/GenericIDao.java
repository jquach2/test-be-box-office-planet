package fr.eql.ai110.bebop.idao;

import java.util.List;

/**
 * 
 * Adds operations that are not entity specific for direct use at Business level.
 * Methods are implemented in GenericDao abstract class.
 * @author Thibault
 *
 * @param <T>
 * refers to an object type
 */
public interface GenericIDao<T> {

	//CRUD
	/** inserts element in database */
	T add(T t);
	/**  returns list of elements from database */
	List<T> getAll();
	/** returns element with this id from database */
	T getById(int id);
	/** updates element in database */
	T update(T t);
	/** deletes element in database */
	boolean delete(T t);
}
