package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Reward;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface RewardIDao extends GenericIDao<Reward> {

	/** returns all rewards that contain these characters in name */
	List<Reward> getRewardsByName(String name);
	
	/** returns the total count of gifts */
	Long countRewards();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
