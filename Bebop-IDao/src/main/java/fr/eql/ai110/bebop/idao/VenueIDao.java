package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Venue;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface VenueIDao extends GenericIDao<Venue> {

	/** returns all venues that contain these characters in name */
	List<Venue> getVenuesByName(String name);
	
	/** returns the total count of venues */
	Long countVenues();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
