package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Category;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface CategoryIDao extends GenericIDao<Category> {

	/** returns all categories that contain these characters in name */
	List<Category> getCategoriesByName(String name);
	
	/** returns the total count of categories */
	Long countCategories();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
