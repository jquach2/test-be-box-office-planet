package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Country;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface CountryIDao extends GenericIDao<Country> {

	/** returns all countries that contain these characters in name */
	List<Country> getCountriesByName(String name);
	
	/** returns the total count of countries */
	Long countCountries();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
