package fr.eql.ai110.bebop.idao;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Schedule;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface ScheduleIDao extends GenericIDao<Schedule> {
	
	/** returns the total count of schedules */
	Long countSchedules();
}
