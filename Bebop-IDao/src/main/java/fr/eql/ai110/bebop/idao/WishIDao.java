package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.Wish;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface WishIDao extends GenericIDao<Wish> {

	/** returns all wishes that contain these characters in name */
	List<Wish> getWishesByName(String name);
	
	/** returns the total count of wishes */
	Long countWishes();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
