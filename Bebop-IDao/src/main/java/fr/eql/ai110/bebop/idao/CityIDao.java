package fr.eql.ai110.bebop.idao;

import java.util.List;

//Imports specific Entity to work with
import fr.eql.ai110.bebop.entity.City;

/**
 * 
 * Adds entity specific operations to implement later at Dao level 
 * and extends generic operations.
 * @author Thibault
 *
 */
public interface CityIDao extends GenericIDao<City> {

	/** returns all cities that contain these characters in name */
	List<City> getCitiesByName(String name);
	
	/** returns the total count of cities */
	Long countCities();
	
	/** checks if name is already taken */
	boolean isNameAvailable(String name);
	
}
