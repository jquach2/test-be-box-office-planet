#!/bin/bash

#VARIABLES
VOLUME_NAME=bebop-mysql-volume

echo "*** Stopping Deployment ***"
docker compose down
mvn clean
rm module.xml

echo "*** Removing residual elements ***"
echo "WARNING! Do you wish to remove the docker persitent volume $VOLUME_NAME? (all data will be lost) "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no volume was removed "
else
	docker volume rm $VOLUME_NAME
fi
echo "Do you wish to remove any present Java Archive? "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no file was removed "
else
	rm -Rf *.jar
fi
echo "Do you wish to remove any present Enterprise Archive? "
read -p "(y/n) ? " commit
if [ ${commit:=n} != "y" ]
        then
	echo "no file was removed "
else
	rm -Rf *.ear
fi
