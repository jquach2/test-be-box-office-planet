package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * Reward is a single instance of a same reward type
 * @author Thibault 
 *
 */
@Entity
@Table(name="reward")
public class Reward implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "info")
	private String info;
	@Column(name = "redeem_code")
	private String redeemCode;
	@Column(name = "purchase_time")
	private LocalDateTime purchaseTime;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Client client;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private RewardType rewardType;

	//CONSTRUCTORS
	public Reward() {
		super();
	}

	public Reward(Integer id, String info, String redeemCode, LocalDateTime purchaseTime, Client client,
			RewardType rewardType) {
		super();
		this.id = id;
		this.info = info;
		this.redeemCode = redeemCode;
		this.purchaseTime = purchaseTime;
		this.client = client;
		this.rewardType = rewardType;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getRedeemCode() {
		return redeemCode;
	}

	public void setRedeemCode(String redeemCode) {
		this.redeemCode = redeemCode;
	}

	public LocalDateTime getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(LocalDateTime purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public RewardType getRewardType() {
		return rewardType;
	}

	public void setRewardType(RewardType rewardType) {
		this.rewardType = rewardType;
	}
}

