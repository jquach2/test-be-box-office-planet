package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Space is a performance space made available by the venue 
 * @author Thibault 
 *
 */
@Entity
@Table(name="space")
public class Space implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "capacity")
	private Integer capacity;
	
	@OneToMany(mappedBy = "space", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Event> events;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Venue venue;

	//CONSTRUCTORS
	public Space() {
		super();
	}

	public Space(Integer id, String name, Integer capacity, List<Event> events, Venue venue) {
		super();
		this.id = id;
		this.name = name;
		this.capacity = capacity;
		this.events = events;
		this.venue = venue;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
}

