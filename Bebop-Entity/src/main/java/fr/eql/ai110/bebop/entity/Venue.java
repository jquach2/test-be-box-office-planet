package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Venue is the location where an event is hosted
 * @author Thibault 
 *
 */
@Entity
@Table(name="venue")
public class Venue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "address")
	private String address;
	@Column(name = "picture")
	private String picture;
	@Column(name = "description")
	private String description;
	@Column(name = "homepage")
	private String homepage;

	@OneToMany(mappedBy = "venue", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Space> spaces;
	
	@OneToMany(mappedBy = "venue", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Schedule> schedules;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private City city;

	//CONSTRUCTORS
	public Venue() {
		super();
	}

	public Venue(Integer id, String name, String address, String picture, String description, String homepage,
			List<Space> spaces, List<Schedule> schedules, City city) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.picture = picture;
		this.description = description;
		this.homepage = homepage;
		this.spaces = spaces;
		this.schedules = schedules;
		this.city = city;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public List<Space> getSpaces() {
		return spaces;
	}

	public void setSpaces(List<Space> spaces) {
		this.spaces = spaces;
	}

	public List<Schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}

