package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 
 * Ticket is a unique entry to an event
 * @author Thibault 
 *
 */
@Entity
@Table(name="Ticket")
public class Ticket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "purchase_time")
	private LocalDateTime purchaseTime;
	@Column(name = "refund_time")
	private LocalDateTime refundTime;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Client client;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Event event;
	
	//CONSTRUCTORS
	public Ticket() {
		super();
	}

	public Ticket(Integer id, String firstName, String lastName, LocalDateTime purchaseTime, LocalDateTime refundTime,
			Client client, Event event) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.purchaseTime = purchaseTime;
		this.refundTime = refundTime;
		this.client = client;
		this.event = event;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDateTime getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(LocalDateTime purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public LocalDateTime getRefundTime() {
		return refundTime;
	}

	public void setRefundTime(LocalDateTime refundTime) {
		this.refundTime = refundTime;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
}

