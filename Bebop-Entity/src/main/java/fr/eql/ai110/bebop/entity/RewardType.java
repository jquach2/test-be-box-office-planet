package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * RewardType is a collection of rewards made available through the exchange of points 
 * @author Thibault 
 *
 */
@Entity
@Table(name="reward_type")
public class RewardType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "points")
	private Integer points;
	@Column(name = "description")
	private String description;
	@Column(name = "picture")
	private String picture;

	@OneToMany(mappedBy = "rewardType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Reward> rewards;
	
	//CONSTRUCTORS
	public RewardType() {
		super();
	}

	//GETTERS & SETTERS
	public RewardType(Integer id, String name, Integer points, String description, String picture) {
		super();
		this.id = id;
		this.name = name;
		this.points = points;
		this.description = description;
		this.picture = picture;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}