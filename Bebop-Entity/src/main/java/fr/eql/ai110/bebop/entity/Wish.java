package fr.eql.ai110.bebop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * Wish is a single instance of the client event wishlist
 * @author Thibault 
 *
 */
@Entity
@Table(name="wish")
public class Wish implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Client client;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Event event;

	//CONSTRUCTORS
	public Wish() {
		super();
	}

	public Wish(Integer id, Client client, Event event) {
		super();
		this.id = id;
		this.client = client;
		this.event = event;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
}

