package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Client is a user with front office functionalities available for use
 * @author Thibault 
 *
 */
@Entity
@Table(name="client")
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "birthdate")
	private LocalDate birthdate;
	@Column(name = "email")
	private String email;
	@Column(name = "phone")
	private String phone;
	@Column(name = "create_time")
	private LocalDateTime createTime;
	@Column(name = "deactivation_time")
	private LocalDateTime deactivationTime;
	@Column(name = "validation_code")
	private String validationCode;
	@Column(name = "validation_time")
	private LocalDateTime validationTime;
	@Column(name = "points")
	private Integer points;
	@Column(name = "permanently_delete")
	private LocalDateTime permanentlyDelete;
	
	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Reward> rewards;
	
	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Ticket> tickets;
	
	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Wish> wishList;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private City city;
	
	//CONSTRUCTORS
	public Client() {	
		super();
	}

	public Client(Integer id, String username, String password, String firstName, String lastName, LocalDate birthdate,
			String email, String phone, LocalDateTime createTime, LocalDateTime deactivationTime, String validationCode,
			LocalDateTime validationTime, Integer points, LocalDateTime permanentlyDelete, List<Reward> rewards,
			List<Ticket> tickets, List<Wish> wishList, City city) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.email = email;
		this.phone = phone;
		this.createTime = createTime;
		this.deactivationTime = deactivationTime;
		this.validationCode = validationCode;
		this.validationTime = validationTime;
		this.points = points;
		this.permanentlyDelete = permanentlyDelete;
		this.rewards = rewards;
		this.tickets = tickets;
		this.wishList = wishList;
		this.city = city;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public LocalDateTime getDeactivationTime() {
		return deactivationTime;
	}

	public void setDeactivationTime(LocalDateTime deactivationTime) {
		this.deactivationTime = deactivationTime;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public LocalDateTime getValidationTime() {
		return validationTime;
	}

	public void setValidationTime(LocalDateTime validationTime) {
		this.validationTime = validationTime;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public LocalDateTime getPermanentlyDelete() {
		return permanentlyDelete;
	}

	public void setPermanentlyDelete(LocalDateTime permanentlyDelete) {
		this.permanentlyDelete = permanentlyDelete;
	}

	public List<Reward> getRewards() {
		return rewards;
	}

	public void setRewards(List<Reward> rewards) {
		this.rewards = rewards;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public List<Wish> getWishList() {
		return wishList;
	}

	public void setWishList(List<Wish> wishList) {
		this.wishList = wishList;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}




