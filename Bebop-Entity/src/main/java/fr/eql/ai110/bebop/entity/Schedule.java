package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Schedule refers to the opening hours of a venue
 * @author Thibault 
 *
 */
@Entity
@Table(name="schedule")
public class Schedule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "opening_time")
	private LocalDateTime openingTime;
	@Column(name = "closing_time")
	private LocalDateTime closingTime;
	
	@OneToMany(mappedBy = "schedule", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Day> days;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Venue venue;

	//CONSTRUCTORS
	public Schedule() {
		super();
	}

	public Schedule(Integer id, LocalDateTime openingTime, LocalDateTime closingTime) {
		super();
		this.id = id;
		this.openingTime = openingTime;
		this.closingTime = closingTime;
	}

	//GETTERS & SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(LocalDateTime openingTime) {
		this.openingTime = openingTime;
	}

	public LocalDateTime getClosingTime() {
		return closingTime;
	}

	public void setClosingTime(LocalDateTime closingTime) {
		this.closingTime = closingTime;
	}
}

