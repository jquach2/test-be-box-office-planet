package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Partner is a user with access to all functionalities related to the organization of events
 * @author Thibault 
 *
 */
@Entity
@Table(name="partner")
public class Partner implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "birthdate")
	private LocalDate birthdate;
	@Column(name = "organization")
	private String organization;
	@Column(name = "title")
	private String title;
	@Column(name = "picture")
	private String picture;
	@Column(name = "description")
	private String description;
	@Column(name = "email")
	private String email;
	@Column(name = "phone")
	private String phone;
	@Column(name = "create_time")
	private LocalDateTime createTime;
	@Column(name = "deactivation_time")
	private LocalDateTime deactivationTime;
	@Column(name = "validation_code")
	private String validationCode;
	@Column(name = "validation_time")
	private LocalDateTime validationTime;
	@Column(name = "admin_approval")
	private LocalDateTime adminApproval;
	@Column(name = "permanently_delete")
	private LocalDateTime permanentlyDelete;
	
	
	@OneToMany(mappedBy = "partner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Event> events;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private City city;

	//CONSTRUCTORS
	public Partner() {
		super();
	}

	public Partner(Integer id, String username, String password, String firstName, String lastName, LocalDate birthdate,
			String organization, String title, String picture, String description, String email, String phone,
			LocalDateTime createTime, LocalDateTime deactivationTime, String validationCode,
			LocalDateTime validationTime, LocalDateTime adminApproval, LocalDateTime permanentlyDelete,
			List<Event> events, City city) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.organization = organization;
		this.title = title;
		this.picture = picture;
		this.description = description;
		this.email = email;
		this.phone = phone;
		this.createTime = createTime;
		this.deactivationTime = deactivationTime;
		this.validationCode = validationCode;
		this.validationTime = validationTime;
		this.adminApproval = adminApproval;
		this.permanentlyDelete = permanentlyDelete;
		this.events = events;
		this.city = city;
	}

	//GETTTERS & SETTERS
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public LocalDateTime getDeactivationTime() {
		return deactivationTime;
	}

	public void setDeactivationTime(LocalDateTime deactivationTime) {
		this.deactivationTime = deactivationTime;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public LocalDateTime getValidationTime() {
		return validationTime;
	}

	public void setValidationTime(LocalDateTime validationTime) {
		this.validationTime = validationTime;
	}

	public LocalDateTime getAdminApproval() {
		return adminApproval;
	}

	public void setAdminApproval(LocalDateTime adminApproval) {
		this.adminApproval = adminApproval;
	}

	public LocalDateTime getPermanentlyDelete() {
		return permanentlyDelete;
	}

	public void setPermanentlyDelete(LocalDateTime permanentlyDelete) {
		this.permanentlyDelete = permanentlyDelete;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}

