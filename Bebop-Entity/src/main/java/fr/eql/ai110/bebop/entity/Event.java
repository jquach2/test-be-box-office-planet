package fr.eql.ai110.bebop.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * 
 * Event is an element featured in the main catalog and search feature
 * @author Thibault 
 *
 */
@Entity
@Table(name="event")
public class Event implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//ATTRIBUTES
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "title")
	private String title;
	@Column(name = "description")
	private String description;
	@Column(name = "price")
	private float price;
	@Column(name = "points")
	private Integer points;
	@Column(name = "date")
	private LocalDate date;
	@Column(name = "starting_time")
	private LocalTime startingTime;
	@Column(name = "length")
	private Integer length;
	@Column(name = "picture")
	private String picture;
	
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Ticket> tickets;
	
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Wish> wishList;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Partner partner;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Space space;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Subset subset;
	
	//CONSTRUCTORS
	public Event() {
		super();
	}

	public Event(Integer id, String title, String description, float price, Integer points, LocalDate date,
			LocalTime startingTime, Integer length, String picture, List<Ticket> tickets, List<Wish> wishList,
			Partner partner, Space space, Subset subset) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.price = price;
		this.points = points;
		this.date = date;
		this.startingTime = startingTime;
		this.length = length;
		this.picture = picture;
		this.tickets = tickets;
		this.wishList = wishList;
		this.partner = partner;
		this.space = space;
		this.subset = subset;
	}

	// GETTERS & SETTERS
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartingTime() {
		return startingTime;
	}

	public void setStartingTime(LocalTime startingTime) {
		this.startingTime = startingTime;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public List<Wish> getWishList() {
		return wishList;
	}

	public void setWishList(List<Wish> wishList) {
		this.wishList = wishList;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Space getSpace() {
		return space;
	}

	public void setSpace(Space space) {
		this.space = space;
	}

	public Subset getSubset() {
		return subset;
	}

	public void setSubset(Subset subset) {
		this.subset = subset;
	}
}