#!/bin/bash

#VARIABLES
MYSQL_CONNECTOR=mysql-connector-java
MYSQL_VERSION=8.0.28
ENTERPRISE_ARCHIVE=Bebop-Ear
EAR_PATH=Bebop-Ear/target
EAR_VERSION=1.0.0
NAMESPACE=thibaultcharrin
IMAGE=be-box-office-planet
TAG=latest

echo "*** Fetching MySQL Java Connector ***"
if [ -f $MYSQL_CONNECTOR-$MYSQL_VERSION.jar ]
then
	echo "Using existing MySQL Java Connector"
else
	curl -LO https://downloads.mysql.com/archives/get/p/3/file/$MYSQL_CONNECTOR-$MYSQL_VERSION.tar.gz
	tar -xvf $MYSQL_CONNECTOR-$MYSQL_VERSION.tar.gz
	cp $MYSQL_CONNECTOR-$MYSQL_VERSION/$MYSQL_CONNECTOR-$MYSQL_VERSION.jar .
	rm -Rf $MYSQL_CONNECTOR-$MYSQL_VERSION/
	rm $MYSQL_CONNECTOR-$MYSQL_VERSION.tar.gz
fi
echo "Accepting MySQL Connector"
cp module-template.xml module.xml
sed -i "s/\${MYSQL_CONNECTOR}/${MYSQL_CONNECTOR}-${MYSQL_VERSION}/g" module.xml

echo "*** Building App ***"
if [ -f $ENTERPRISE_ARCHIVE-$EAR_VERSION.ear ]
then
	echo "Using existing Enterprise Archive"
    if [ `docker images | grep $NAMESPACE/$IMAGE | wc -l` == 0 ]
    then
        echo "Creating image from current Enterprise Archive"
	    docker build -t "$NAMESPACE/$IMAGE:$TAG" .
    else
        echo "Using current Docker image"
    fi
else
	mvn clean install
    cp $EAR_PATH/$ENTERPRISE_ARCHIVE-$EAR_VERSION.ear .
    echo "*** Building Image ***"
    if [ `docker images | grep $NAMESPACE/$IMAGE | wc -l` == 1 ]
    then
        echo "Removing current image"
	    docker rmi -f $NAMESPACE/$IMAGE:$TAG
    else
        echo "Creating image from new Enterprise Archive"
    fi
    docker build -t "$NAMESPACE/$IMAGE:$TAG" .
fi

echo "*** Deploying App ***"
docker compose up -d
