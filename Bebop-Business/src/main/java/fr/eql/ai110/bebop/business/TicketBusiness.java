package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Ticket;
import fr.eql.ai110.bebop.ibusiness.TicketIBusiness;
import fr.eql.ai110.bebop.idao.TicketIDao;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

@Remote(TicketIBusiness.class)
@Stateless
public class TicketBusiness implements TicketIBusiness {

	@EJB
	private TicketIDao ticketDao;

	//CREATE
	@Override
	public boolean createTicket(Ticket ticket) {
		//TODO createTicket needs control layer
		ticketDao.add(ticket);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//READ
	@Override
	public List<Ticket> findAllTickets() {
		//TODO getAll control layer
		return ticketDao.getAll();
	}

	@Override
	public List<Ticket> findTicketsByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return ticketDao.getTicketsByName(name);
	}

	@Override
	public Ticket findTicketById(Integer id) {
		// TODO getById control layer
		return ticketDao.getById(id);
	}

	@Override
	public Long findNbTickets() {
		Long result = ticketDao.countTickets();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateTicket(Ticket ticket) {
		//TODO updateTicket needs control layer
		ticketDao.update(ticket);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//DELETE
	@Override
	public boolean deleteTicket(Ticket ticket) {
		int id = ticket.getId();
		boolean verify = ticketDao.getById(id)!=null;
		if (verify) ticketDao.delete(ticket);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
