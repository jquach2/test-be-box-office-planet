package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Event;
import fr.eql.ai110.bebop.ibusiness.EventIBusiness;
import fr.eql.ai110.bebop.idao.EventIDao;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */


@Remote(EventIBusiness.class)
@Stateless
public class EventBusiness implements EventIBusiness {

	@EJB
	private EventIDao eventDao;

	//CREATE
	@Override
	public boolean createEvent(Event event) {
		String myName = event.getTitle();
		boolean verify = eventDao.isNameAvailable(myName);
		if (verify) eventDao.add(event);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Event> findAllEvents() {
		//TODO getAll control layer
		return eventDao.getAll();
	}

	@Override
	public List<Event> findEventsByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return eventDao.getEventsByName(name);
	}

	@Override
	public Event findEventById(Integer id) {
		// TODO getById control layer
		return eventDao.getById(id);
	}

	@Override
	public Long findNbEvents() {
		Long result = eventDao.countEvents();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateEvent(Event event) {
		String myName = event.getTitle();
		boolean verify = eventDao.isNameAvailable(myName);
		if (verify) eventDao.update(event);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteEvent(Event event) {
		int id = event.getId();
		boolean verify = eventDao.getById(id)!=null;
		if (verify) eventDao.delete(event);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
