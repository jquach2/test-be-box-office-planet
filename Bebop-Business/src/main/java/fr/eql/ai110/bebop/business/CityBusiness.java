package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.City;
import fr.eql.ai110.bebop.ibusiness.CityIBusiness;
import fr.eql.ai110.bebop.idao.CityIDao;

@Remote(CityIBusiness.class)
@Stateless
public class CityBusiness implements CityIBusiness {

	@EJB
	private CityIDao cityDao;

	//CREATE
	@Override
	public boolean createCity(City city) {
		String myName = city.getName();
		boolean verify = cityDao.isNameAvailable(myName);
		if (verify) cityDao.add(city);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<City> findAllCities() {
		//TODO getAll control layer
		return cityDao.getAll();
	}

	@Override
	public List<City> findCitiesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return cityDao.getCitiesByName(name);
	}

	@Override
	public City findCityById(Integer id) {
		// TODO getById control layer
		return cityDao.getById(id);
	}

	@Override
	public Long findNbCities() {
		Long result = cityDao.countCities();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateCity(City city) {
		String myName = city.getName();
		boolean verify = cityDao.isNameAvailable(myName);
		if (verify) cityDao.update(city);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteCity(City city) {
		int id = city.getId();
		boolean verify = cityDao.getById(id)!=null;
		if (verify) cityDao.delete(city);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
