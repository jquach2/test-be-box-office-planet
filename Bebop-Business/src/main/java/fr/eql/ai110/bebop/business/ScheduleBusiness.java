package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Schedule;
import fr.eql.ai110.bebop.ibusiness.ScheduleIBusiness;
import fr.eql.ai110.bebop.idao.ScheduleIDao;

@Remote(ScheduleIBusiness.class)
@Stateless
public class ScheduleBusiness implements ScheduleIBusiness {

	@EJB
	private ScheduleIDao scheduleDao;

	//CREATE
	@Override
	public boolean createSchedule(Schedule schedule) {
		//TODO createSchedule needs control layer
		scheduleDao.add(schedule);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//READ
	@Override
	public List<Schedule> findAllSchedules() {
		//TODO getAll control layer
		return scheduleDao.getAll();
	}

	@Override
	public Schedule findScheduleById(Integer id) {
		// TODO getById control layer
		return scheduleDao.getById(id);
	}

	@Override
	public Long findNbSchedules() {
		Long result = scheduleDao.countSchedules();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateSchedule(Schedule schedule) {
		//TODO updateSchedule needs control layer
		scheduleDao.update(schedule);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//DELETE
	@Override
	public boolean deleteSchedule(Schedule schedule) {
		int id = schedule.getId();
		boolean verify = scheduleDao.getById(id)!=null;
		if (verify) scheduleDao.delete(schedule);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
