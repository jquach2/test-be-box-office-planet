package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Wish;
import fr.eql.ai110.bebop.ibusiness.WishIBusiness;
import fr.eql.ai110.bebop.idao.WishIDao;

@Remote(WishIBusiness.class)
@Stateless
public class WishBusiness implements WishIBusiness {

	@EJB
	private WishIDao wishDao;

	//CREATE
	@Override
	public boolean createWish(Wish wish) {
		//TODO createWish needs control layer
		wishDao.add(wish);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//READ
	@Override
	public List<Wish> findAllWishes() {
		//TODO getAll control layer
		return wishDao.getAll();
	}

	@Override
	public List<Wish> findWishesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return wishDao.getWishesByName(name);
	}

	@Override
	public Wish findWishById(Integer id) {
		// TODO getById control layer
		return wishDao.getById(id);
	}

	@Override
	public Long findNbWishes() {
		Long result = wishDao.countWishes();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateWish(Wish wish) {
		//TODO updateWish needs control layer
		wishDao.update(wish);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return true;
	}

	//DELETE
	@Override
	public boolean deleteWish(Wish wish) {
		int id = wish.getId();
		boolean verify = wishDao.getById(id)!=null;
		if (verify) wishDao.delete(wish);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
