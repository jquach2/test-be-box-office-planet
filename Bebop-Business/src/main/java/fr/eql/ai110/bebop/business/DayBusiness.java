package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Day;
import fr.eql.ai110.bebop.ibusiness.DayIBusiness;
import fr.eql.ai110.bebop.idao.DayIDao;

@Remote(DayIBusiness.class)
@Stateless
public class DayBusiness implements DayIBusiness {

	@EJB
	private DayIDao dayDao;

	//CREATE
	@Override
	public boolean createDay(Day day) {
		String myName = day.getName();
		boolean verify = dayDao.isNameAvailable(myName);
		if (verify) dayDao.add(day);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Day> findAllDays() {
		//TODO getAll control layer
		return dayDao.getAll();
	}

	@Override
	public List<Day> findDaysByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return dayDao.getDaysByName(name);
	}

	@Override
	public Day findDayById(Integer id) {
		// TODO getById control layer
		return dayDao.getById(id);
	}

	@Override
	public Long findNbDays() {
		Long result = dayDao.countDays();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateDay(Day day) {
		String myName = day.getName();
		boolean verify = dayDao.isNameAvailable(myName);
		if (verify) dayDao.update(day);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteDay(Day day) {
		int id = day.getId();
		boolean verify = dayDao.getById(id)!=null;
		if (verify) dayDao.delete(day);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
