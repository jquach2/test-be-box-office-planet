package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Subset;
import fr.eql.ai110.bebop.ibusiness.SubsetIBusiness;
import fr.eql.ai110.bebop.idao.SubsetIDao;



/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */


@Remote(SubsetIBusiness.class)
@Stateless
public class SubsetBusiness implements SubsetIBusiness {

	@EJB
	private SubsetIDao subsetDao;

	//CREATE
	@Override
	public boolean createSubset(Subset subset) {
		String myName = subset.getName();
		boolean verify = subsetDao.isNameAvailable(myName);
		if (verify) subsetDao.add(subset);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Subset> findAllSubsets() {
		//TODO getAll control layer
		return subsetDao.getAll();
	}

	@Override
	public List<Subset> findSubsetsByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return subsetDao.getSubsetsByName(name);
	}

	@Override
	public Subset findSubsetById(Integer id) {
		// TODO getById control layer
		return subsetDao.getById(id);
	}

	@Override
	public Long findNbSubsets() {
		Long result = subsetDao.countSubsets();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateSubset(Subset subset) {
		String myName = subset.getName();
		boolean verify = subsetDao.isNameAvailable(myName);
		if (verify) subsetDao.update(subset);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteSubset(Subset subset) {
		int id = subset.getId();
		boolean verify = subsetDao.getById(id)!=null;
		if (verify) subsetDao.delete(subset);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
