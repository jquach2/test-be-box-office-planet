package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Country;
import fr.eql.ai110.bebop.ibusiness.CountryIBusiness;
import fr.eql.ai110.bebop.idao.CountryIDao;

@Remote(CountryIBusiness.class)
@Stateless
public class CountryBusiness implements CountryIBusiness {

	@EJB
	private CountryIDao countryDao;

	//CREATE
	@Override
	public boolean createCountry(Country country) {
		String myName = country.getName();
		boolean verify = countryDao.isNameAvailable(myName);
		if (verify) countryDao.add(country);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Country> findAllCountries() {
		//TODO getAll control layer
		return countryDao.getAll();
	}

	@Override
	public List<Country> findCountriesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return countryDao.getCountriesByName(name);
	}

	@Override
	public Country findCountryById(Integer id) {
		// TODO getById control layer
		return countryDao.getById(id);
	}

	@Override
	public Long findNbCountries() {
		Long result = countryDao.countCountries();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateCountry(Country country) {
		String myName = country.getName();
		boolean verify = countryDao.isNameAvailable(myName);
		if (verify) countryDao.update(country);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteCountry(Country country) {
		int id = country.getId();
		boolean verify = countryDao.getById(id)!=null;
		if (verify) countryDao.delete(country);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
