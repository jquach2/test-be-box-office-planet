package fr.eql.ai110.bebop.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Partner;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.ibusiness.PartnerIBusiness;
import fr.eql.ai110.bebop.idao.AdminIDao;
import fr.eql.ai110.bebop.idao.ClientIDao;
import fr.eql.ai110.bebop.idao.PartnerIDao;

@Remote(PartnerIBusiness.class)
@Stateless
public class PartnerBusiness implements PartnerIBusiness {

	@EJB
	private PartnerIDao partnerDao;

	@EJB
	private AdminIDao adminDao;

	@EJB
	private ClientIDao clientDao;

	private final static Integer MIN_LENGTH = 2;
	private final static Integer MAX_LENGTH = 30;

	//AUTHENTICATION
	@Override
	public String hashPassword(String password) {
		String hashedPassword = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	@Override
	public Partner connectPartner(Partner partner) {
		String username = partner.getUsername();
		String password = hashPassword(partner.getPassword());
		return partnerDao.partnerAuthenticate(username, password);
	}

	//CREATE
	@Override
	public boolean createPartner(Partner partner) {
		String myName = partner.getUsername();
		String myPassword = partner.getPassword();
		boolean verify = myName.length()>MIN_LENGTH
				&&myName.length()<MAX_LENGTH
				&&myPassword.length()>MIN_LENGTH
				&&myPassword.length()<MAX_LENGTH
				&&partnerDao.isNameAvailable(myName)
				&&adminDao.isNameAvailable(myName)
				&&clientDao.isNameAvailable(myName);
				if (verify) {
					partner.setPassword(hashPassword(myPassword));
					partnerDao.add(partner);
				}
				/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
				 */
				return verify;
	}

	//READ
	@Override
	public List<Partner> findAllPartners() {
		return partnerDao.getAll();
	}

	@Override
	public List<Partner> findPartnersByMulti(Partner partner) {
		String username = partner.getUsername();
		String email = partner.getEmail();
		String firstName = partner.getFirstName();
		String lastName = partner.getLastName();
		return partnerDao.getPartnersByMulti(username, email, firstName, lastName);
	}

	@Override
	public List<Partner> findPartnersByName(String name) {
		return partnerDao.getPartnersByName(name);
	}

	@Override
	public Partner findPartnerById(Integer id) {
		return partnerDao.getById(id);
	}

	@Override
	public Long findNbPartners() {
		Long result = partnerDao.countPartners();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updatePartner(Partner partner) {
		boolean newUsernameVerify = true;
		boolean newPasswordVerify = true;

		String oldUsername = partnerDao.getById(partner.getId()).getUsername();
		String newUsername = partner.getUsername();
		String oldPassword = partnerDao.getById(partner.getId()).getPassword();
		String newPassword = partner.getPassword();

		if (!oldUsername.equals(newUsername)) {
			newUsernameVerify = newUsername.length()>MIN_LENGTH
					&&newUsername.length()<MAX_LENGTH
					&&partnerDao.isNameAvailable(newUsername)
					&&adminDao.isNameAvailable(newUsername)
					&&clientDao.isNameAvailable(newUsername);
		}

		if (!oldPassword.equals(newPassword)) {
			newPasswordVerify = newPassword.length()>MIN_LENGTH
					&&newPassword.length()<MAX_LENGTH;
			partner.setPassword(hashPassword(newPassword));
		}
		
		boolean allConditionsVerified = newUsernameVerify&&newPasswordVerify;
		
		if (allConditionsVerified) {
			partnerDao.update(partner);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return allConditionsVerified;
	}

	//DELETE
	@Override
	public boolean deletePartner(Partner partner) {
		boolean verify = partner.getUsername()!=null
				&&partner.getPermanentlyDelete()!=null;
		if (verify) {
			partner.setAdminApproval(null);
			partner.setBirthdate(null);
			partner.setCreateTime(null);
			partner.setDeactivationTime(null);
			partner.setDescription(null);
			partner.setEmail(null);
			partner.setFirstName(null);
			partner.setLastName(null);
			partner.setOrganization(null);
			partner.setPassword(null);
			partner.setPhone(null);
			partner.setPicture(null);
			partner.setTitle(null);
			partner.setUsername(null);
			partner.setValidationCode(null);
			partner.setValidationTime(null);
			partnerDao.update(partner);
		} 
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
