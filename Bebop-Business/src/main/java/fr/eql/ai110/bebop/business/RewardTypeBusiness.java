package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.RewardType;
import fr.eql.ai110.bebop.ibusiness.RewardTypeIBusiness;
import fr.eql.ai110.bebop.idao.RewardTypeIDao;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

@Remote(RewardTypeIBusiness.class)
@Stateless
public class RewardTypeBusiness implements RewardTypeIBusiness {

	@EJB
	private RewardTypeIDao rewardTypeDao;
	
	private final static Integer MIN_LENGTH = 2;

	//CREATE
	@Override
	public boolean createRewardType(RewardType rewardType) {
		String myName = rewardType.getName();
		Integer myPoints = rewardType.getPoints();
		boolean verify = myName.length()>MIN_LENGTH
				&& myPoints!=null
				&& rewardTypeDao.isNameAvailable(myName);
		if (verify) rewardTypeDao.add(rewardType);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<RewardType> findAllRewardTypes() {
		return rewardTypeDao.getAll();
	}

	@Override
	public List<RewardType> findRewardTypesByName(String name) {
		return rewardTypeDao.getRewardTypesByName(name);
	}

	@Override
	public RewardType findRewardTypeById(Integer id) {
		return rewardTypeDao.getById(id);
	}

	@Override
	public Long findNbRewardTypes() {
		Long result = rewardTypeDao.countRewardTypes();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateRewardType(RewardType rewardType) {
		String oldName = rewardTypeDao.getById(rewardType.getId()).getName();
		String newName = rewardType.getName();
		Integer newPoints = rewardType.getPoints();
		boolean verify = newName.length()>MIN_LENGTH&&newPoints!=null;
		boolean newNameVerify = true;
		if (!oldName.equals(newName)) {
			newNameVerify = rewardTypeDao.isNameAvailable(newName);
		}
		if (verify&&newNameVerify) {
			rewardTypeDao.update(rewardType);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify&&newNameVerify;
	}

	//DELETE
	@Override
	public boolean deleteRewardType(RewardType rewardType) {
		boolean verify = rewardType.getName()!=null;
		if (verify) {
			rewardType.setDescription(null);
			rewardType.setName(null);
			rewardType.setPicture(null);
			rewardType.setPoints(null);
			rewardTypeDao.update(rewardType);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
