package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.ArchetypeEntity;
import fr.eql.ai110.bebop.ibusiness.ArchetypeIBusiness;
import fr.eql.ai110.bebop.idao.ArchetypeIDao;

@Remote(ArchetypeIBusiness.class)
@Stateless
public class ArchetypeBusiness implements ArchetypeIBusiness {

	@EJB
	private ArchetypeIDao archetypeDao;

	//CREATE
	@Override
	public boolean createArchetypeEntity(ArchetypeEntity archetypeEntity) {
		String myName = archetypeEntity.getName();
		boolean verify = archetypeDao.isNameAvailable(myName);
		if (verify) archetypeDao.add(archetypeEntity);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<ArchetypeEntity> findAllArchetypeEntities() {
		//TODO getAll control layer
		return archetypeDao.getAll();
	}

	@Override
	public List<ArchetypeEntity> findArchetypeEntitiesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return archetypeDao.getArchetypeEntitiesByName(name);
	}

	@Override
	public ArchetypeEntity findArchetypeEntityById(Integer id) {
		// TODO getById control layer
		return archetypeDao.getById(id);
	}

	@Override
	public Long findNbArchetypes() {
		Long result = archetypeDao.countArchetypeEntities();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateArchetypeEntity(ArchetypeEntity archetypeEntity) {
		String myName = archetypeEntity.getName();
		boolean verify = archetypeDao.isNameAvailable(myName);
		if (verify) archetypeDao.update(archetypeEntity);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteArchetypeEntity(ArchetypeEntity archetypeEntity) {
		int id = archetypeEntity.getId();
		boolean verify = archetypeDao.getById(id)!=null;
		if (verify) archetypeDao.delete(archetypeEntity);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
