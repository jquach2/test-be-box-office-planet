package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Space;
import fr.eql.ai110.bebop.ibusiness.SpaceIBusiness;
import fr.eql.ai110.bebop.idao.SpaceIDao;

@Remote(SpaceIBusiness.class)
@Stateless
public class SpaceBusiness implements SpaceIBusiness {

	@EJB
	private SpaceIDao spaceDao;

	//CREATE
	@Override
	public boolean createSpace(Space space) {
		String myName = space.getName();
		boolean verify = spaceDao.isNameAvailable(myName);
		if (verify) spaceDao.add(space);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Space> findAllSpaces() {
		//TODO getAll control layer
		return spaceDao.getAll();
	}

	@Override
	public List<Space> findSpacesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return spaceDao.getSpacesByName(name);
	}

	@Override
	public Space findSpaceById(Integer id) {
		// TODO getById control layer
		return spaceDao.getById(id);
	}

	@Override
	public Long findNbSpaces() {
		Long result = spaceDao.countSpaces();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateSpace(Space space) {
		String myName = space.getName();
		boolean verify = spaceDao.isNameAvailable(myName);
		if (verify) spaceDao.update(space);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteSpace(Space space) {
		int id = space.getId();
		boolean verify = spaceDao.getById(id)!=null;
		if (verify) spaceDao.delete(space);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
