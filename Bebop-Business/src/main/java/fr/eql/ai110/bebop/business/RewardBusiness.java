package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Reward;
import fr.eql.ai110.bebop.ibusiness.RewardIBusiness;
import fr.eql.ai110.bebop.idao.RewardIDao;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */


@Remote(RewardIBusiness.class)
@Stateless
public class RewardBusiness implements RewardIBusiness {

	@EJB
	private RewardIDao rewardDao;

	private final static Integer MIN_LENGTH = 2;

	//CREATE
	@Override
	public boolean createReward(Reward reward) {
		String myInfo = reward.getInfo();
		String myCode = reward.getRedeemCode();
		boolean verify = myInfo.length()>MIN_LENGTH
				&& myCode.length()>MIN_LENGTH
				&& rewardDao.isNameAvailable(myCode);
		if (verify) rewardDao.add(reward);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Reward> findAllRewards() {
		return rewardDao.getAll();
	}

	@Override
	public List<Reward> findRewardsByName(String name) {
		return rewardDao.getRewardsByName(name);
	}

	@Override
	public Reward findRewardById(Integer id) {
		return rewardDao.getById(id);
	}

	@Override
	public Long findNbRewards() {
		Long result = rewardDao.countRewards();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateReward(Reward reward) {
		String oldCode = rewardDao.getById(reward.getId()).getRedeemCode();
		String newCode = reward.getRedeemCode();
		String myInfo = reward.getInfo();
		boolean verify = newCode.length()>MIN_LENGTH&&myInfo.length()>MIN_LENGTH;
		boolean newCodeVerify = true;
		if (!oldCode.equals(newCode)) {
			newCodeVerify = rewardDao.isNameAvailable(newCode);
		}
		if (verify&&newCodeVerify) {
			rewardDao.update(reward);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify&&newCodeVerify;
	}

	//DELETE
	@Override
	public boolean deleteReward(Reward reward) {
		boolean verify = reward.getInfo()!=null;
		if (verify) {
			reward.setInfo(null);
			reward.setPurchaseTime(null);
			reward.setRedeemCode(null);
			rewardDao.update(reward);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
