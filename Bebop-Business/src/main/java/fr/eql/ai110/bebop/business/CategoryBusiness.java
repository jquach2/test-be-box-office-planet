package fr.eql.ai110.bebop.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.entity.Category;
import fr.eql.ai110.bebop.ibusiness.CategoryIBusiness;
import fr.eql.ai110.bebop.idao.CategoryIDao;

@Remote(CategoryIBusiness.class)
@Stateless
public class CategoryBusiness implements CategoryIBusiness {

	@EJB
	private CategoryIDao categoryDao;

	//CREATE
	@Override
	public boolean createCategory(Category category) {
		String myName = category.getName();
		boolean verify = categoryDao.isNameAvailable(myName);
		if (verify) categoryDao.add(category);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//READ
	@Override
	public List<Category> findAllCategories() {
		//TODO getAll control layer
		return categoryDao.getAll();
	}

	@Override
	public List<Category> findCategoriesByName(String name) {
		// TODO getArchetypeEntitiesByName control layer
		return categoryDao.getCategoriesByName(name);
	}

	@Override
	public Category findCategoryById(Integer id) {
		// TODO getById control layer
		return categoryDao.getById(id);
	}

	@Override
	public Long findNbCategories() {
		Long result = categoryDao.countCategories();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateCategory(Category category) {
		String myName = category.getName();
		boolean verify = categoryDao.isNameAvailable(myName);
		if (verify) categoryDao.update(category);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

	//DELETE
	@Override
	public boolean deleteCategory(Category category) {
		int id = category.getId();
		boolean verify = categoryDao.getById(id)!=null;
		if (verify) categoryDao.delete(category);
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return verify;
	}

}
