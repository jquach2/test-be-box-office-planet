package fr.eql.ai110.bebop.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.bebop.entity.Admin;

/*
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 */

import fr.eql.ai110.bebop.ibusiness.AdminIBusiness;
import fr.eql.ai110.bebop.idao.AdminIDao;
import fr.eql.ai110.bebop.idao.ClientIDao;
import fr.eql.ai110.bebop.idao.PartnerIDao;

@Remote(AdminIBusiness.class)
@Stateless
public class AdminBusiness implements AdminIBusiness {

	@EJB
	private AdminIDao adminDao;
	
	@EJB
	private PartnerIDao partnerDao;
	
	@EJB
	private ClientIDao clientDao;

	private final static Integer MIN_LENGTH = 2;
	private final static Integer MAX_LENGTH = 30;

	//AUTHENTICATION
	@Override
	public String hashPassword(String password) {
		String hashedPassword = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	@Override
	public Admin connectAdmin(Admin admin) {
		String username = admin.getUsername();
		String password = hashPassword(admin.getPassword());
		return adminDao.adminAuthenticate(username, password);
	}

	//CREATE
	@Override
	public boolean createAdmin(Admin admin) {
		String myName = admin.getUsername();
		String myPassword = admin.getPassword();
		boolean verify = myName.length()>MIN_LENGTH
				&& myName.length()<MAX_LENGTH
				&& myPassword.length()>MIN_LENGTH
				&& myPassword.length()<MAX_LENGTH				
				&& adminDao.isNameAvailable(myName)
				&& partnerDao.isNameAvailable(myName)
				&& clientDao.isNameAvailable(myName);
				if (verify) {
					admin.setPassword(hashPassword(myPassword));
					adminDao.add(admin);
				}
				/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
				 */
				return verify;
	}

	//READ
	@Override
	public List<Admin> findAllAdmins() {
		return adminDao.getAll();
	}

	@Override
	public List<Admin> findAdminsByMulti(Admin admin) {
		String username = admin.getUsername();
		String email = admin.getEmailRecovery();
		String firstName = admin.getFirstName();
		String lastName = admin.getLastName();
		return adminDao.getAdminsByMulti(username, email, firstName, lastName);
	}

	@Override
	public List<Admin> findAdminsByName(String name) {
		return adminDao.getAdminsByName(name);
	}

	@Override
	public Admin findAdminById(Integer id) {
		return adminDao.getById(id);
	}

	@Override
	public Long findNbAdmins() {
		Long result = adminDao.countAdmins();
		if (result==null) result=0L;
		return result;
	}

	//UPDATE
	@Override
	public boolean updateAdmin(Admin admin) {
		boolean newUsernameVerify = true;
		boolean newPasswordVerify = true;

		String oldUsername = adminDao.getById(admin.getId()).getUsername();
		String newUsername = admin.getUsername();
		String oldPassword = adminDao.getById(admin.getId()).getPassword();
		String newPassword = admin.getPassword();

		if (!oldUsername.equals(newUsername)) {
			newUsernameVerify = newUsername.length()>MIN_LENGTH
					&&newUsername.length()<MAX_LENGTH
					&&partnerDao.isNameAvailable(newUsername)
					&&adminDao.isNameAvailable(newUsername)
					&&clientDao.isNameAvailable(newUsername);
		}

		if (!oldPassword.equals(newPassword)) {
			newPasswordVerify = newPassword.length()>MIN_LENGTH
					&&newPassword.length()<MAX_LENGTH;
			admin.setPassword(hashPassword(newPassword));
		}
		
		boolean allConditionsVerified = newUsernameVerify&&newPasswordVerify;

		if (allConditionsVerified) {
			adminDao.update(admin);
		}
		/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
		 */
		return allConditionsVerified;
	}

	//DELETE
	@Override
	public boolean deleteAdmin(Admin admin) {
		boolean verify = admin.getUsername()!=null
				&&admin.getPermanentlyDelete()!=null
				&&adminDao.countAdmins()>1;
				if (verify) {
					admin.setUsername(null);
					admin.setFirstName(null);
					admin.setLastName(null);
					admin.setEmailRecovery(null);
					admin.setPassword(null);
					admin.setCreateTime(null);
					admin.setDeactivationTime(null);
					adminDao.update(admin);
				} 
				/*
		Logger log = LogManager.getLogger(ArchetypeBusiness.class);
		log.info(message);
				 */
				return verify;
	}
}
