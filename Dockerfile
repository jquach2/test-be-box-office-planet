FROM jboss/wildfly:latest
COPY module.xml $JBOSS_HOME/modules/system/layers/base/com/mysql/main/
COPY *.jar $JBOSS_HOME/modules/system/layers/base/com/mysql/main/
COPY standalone.xml $JBOSS_HOME/standalone/configuration/
COPY *.ear $JBOSS_HOME/standalone/deployments/

