package fr.eql.ai110.bebop.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.bebop.entity.ArchetypeEntity;
import fr.eql.ai110.bebop.ibusiness.ArchetypeIBusiness;

@ManagedBean(name="mbArchetype")
@SessionScoped
public class ArchetypeManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ArchetypeIBusiness archetypeBusiness;

	//ATTRIBUTES
	private String name;
	private String message;
	private ArchetypeEntity archetypeEntity;
	private List<ArchetypeEntity> archetypeEntities;

	private Integer idBuffer;	//Temporarily stores the Id of a Unique Selection, useful for a quick Update or Delete operation


	//METHODS
	@PostConstruct
	public void init() {
		getArchetypes();
	}

	public void getArchetypes() {
		archetypeEntities = archetypeBusiness.findAllArchetypeEntities();
		if (message==null) createMessage(archetypeEntities.size()>0);
	}

	public String createMessage(boolean myCondition) {
		if (myCondition) {
			message = archetypeEntities.size() + " archetype(s) found";
		} else {
			message = "Woops nothing there";
		}
		return message;
	}

	//Create
	public String createOneArchetype() {
		archetypeEntity = new ArchetypeEntity();
		archetypeEntity.setName(name);
		boolean verify = archetypeBusiness.createArchetypeEntity(archetypeEntity);
		if (verify) {
			message = name + " was successfully created";
		} else {
			message = name + " is already taken";
		}
		getArchetypes();
		return message;
	}

	//Read
	public void searchAllArchetypesByName() {
		archetypeEntities = archetypeBusiness.findArchetypeEntitiesByName(name);
		createMessage(archetypeEntities.size()>0);
		if (archetypeEntities.size()==1) {
			idBuffer = archetypeEntities.get(0).getId();
		} else {
			idBuffer = null;
		}
	}

	public void searchOneArchetypeById() {
		archetypeEntity = null;
		try {
			Integer id = Integer.parseInt(name);
			archetypeEntity = archetypeBusiness.findArchetypeEntityById(id);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		if (archetypeEntity!=null) {
			archetypeEntities = new ArrayList<ArchetypeEntity>();
			archetypeEntities.add(archetypeEntity); 
			idBuffer = archetypeEntities.get(0).getId();	//Can only every return a single element
		} else {
			archetypeEntities.clear();
		}
		createMessage(archetypeEntities.size()>0);
	}

	//Update
	public String modifyOneArchetype() {
		if (idBuffer!=null) {
			archetypeEntity.setId(idBuffer);
			archetypeEntity.setName(name);
			boolean verify = archetypeBusiness.updateArchetypeEntity(archetypeEntity);
			if (verify) {
				message = "Was successfully updated to " + name;
			} else {
				message = name + " is already taken";
			}
			idBuffer = null;
			getArchetypes();
		} else {
			searchAllArchetypesByName();
			name = "";
			message = "please select a single archetype to update";
		}
		return message;
	}

	//Remove
	public String removeArchetype() {
		if (idBuffer!=null) {
			archetypeEntity.setId(idBuffer);
			boolean verify = archetypeBusiness.deleteArchetypeEntity(archetypeEntity);
			if (verify) {
				message = "Successfully deleted";
			} else {
				message = "May no longer exist";
			}
			idBuffer = null;
			getArchetypes();
		} else {
			searchAllArchetypesByName();
			name = "";
			message = "please select a single archetype to delete";
		}
		return message;
	}


	//GETTERS & SETTERS
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArchetypeEntity getArchetypeEntity() {
		return archetypeEntity;
	}

	public void setArchetypeEntity(ArchetypeEntity archetypeEntity) {
		this.archetypeEntity = archetypeEntity;
	}

	public List<ArchetypeEntity> getArchetypeEntities() {
		return archetypeEntities;
	}

	public void setArchetypeEntities(List<ArchetypeEntity> archetypeEntities) {
		this.archetypeEntities = archetypeEntities;
	}

	public Integer getIdBuffer() {
		return idBuffer;
	}

	public void setIdBuffer(Integer idBuffer) {
		this.idBuffer = idBuffer;
	}

}
