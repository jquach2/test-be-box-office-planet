package fr.eql.ai110.bebop.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.bebop.entity.Admin;
import fr.eql.ai110.bebop.entity.City;
import fr.eql.ai110.bebop.entity.Client;
import fr.eql.ai110.bebop.entity.Partner;
import fr.eql.ai110.bebop.ibusiness.AdminIBusiness;
import fr.eql.ai110.bebop.ibusiness.CityIBusiness;
import fr.eql.ai110.bebop.ibusiness.ClientIBusiness;
import fr.eql.ai110.bebop.ibusiness.PartnerIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbAuthentication")
@SessionScoped
public class AuthenticationManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AdminIBusiness adminBusiness;

	@EJB
	private PartnerIBusiness partnerBusiness;

	@EJB
	private ClientIBusiness clientBusiness;

	@EJB
	private CityIBusiness cityBusiness;



	// ***** ATTRIBUTES *****

	//Partner

	private LocalDate partnerBirthdate;
	private String partnerDescription;
	private String partnerEmail;
	private String partnerFirstName;
	private String partnerLastName;
	private String partnerOrganization;
	private String partnerPassword;
	private String partnerPhone;
	private String partnerPicture;
	private String partnerTitle;
	private String partnerUsername;
	private String partnerValidationCode;
	private Integer partnerCityFK;

	//Client

	private LocalDate clientBirthdate;
	private String clientEmail;
	private String clientFirstName;
	private String clientLastName;
	private String clientPassword;
	private String clientPhone;
	private Integer clientPoints;
	private String clientUsername;
	private String clientValidationCode;
	private Integer clientCityFK;

	private String dateConvert;	//Stores the date entered as string ready for parsing in dateTransform(String date)

	private Admin admin; 
	private Partner partner;
	private Client client;

	private String username;
	private String password;

	private String loggedUser;

	private City city;
	private List<City> cities;

	private String clientMessage;
	private String partnerMessage;



	// ***** METHODS *****

	@PostConstruct
	public void init() {
		getAllCities();
	}

	public void getAllCities() {
		cities = cityBusiness.findAllCities();
	}

	private LocalDate dateTransform(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		LocalDate localDate = null;
		if (dateConvert.length()==10) {
			localDate = LocalDate.parse(date,formatter);

		}
		return localDate;
	}

	private void clearFields() {
		username = null;
		password = null;
	}



	// *** CREATE ***

	public String createPartner() {
		String forward = "/login.xhtml?faces-redirection=true";
		Partner partner = new Partner();
		partnerBirthdate = dateTransform(dateConvert);
		partner.setBirthdate(partnerBirthdate);
		partner.setCity(cityBusiness.findCityById(partnerCityFK));
		partner.setCreateTime(LocalDateTime.now());
		partner.setDescription(partnerDescription);
		partner.setEmail(partnerEmail);
		partner.setFirstName(partnerFirstName);
		partner.setLastName(partnerLastName);
		partner.setOrganization(partnerOrganization);
		partner.setPassword(partnerPassword);
		partner.setPhone(partnerPhone);
		partner.setPicture(partnerPicture);
		partner.setTitle(partnerTitle);
		partner.setUsername(partnerUsername);
		partner.setValidationCode(partnerValidationCode);
		boolean verify = partnerBusiness.createPartner(partner);	
		if (verify) {
			FacesMessages.info("Votre compte " + partnerUsername + " a bien été créé :) ");
		} else {
			FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
			forward = null;
		} 
		return forward;
	}

	public String createClient() {
		String forward = "/login.xhtml?faces-redirection=true";
		Client client = new Client();
		clientBirthdate = dateTransform(dateConvert);
		client.setBirthdate(clientBirthdate);
		client.setCity(cityBusiness.findCityById(clientCityFK));
		client.setCreateTime(LocalDateTime.now());
		client.setEmail(clientEmail);
		client.setFirstName(clientFirstName);
		client.setLastName(clientLastName);
		client.setPassword(clientPassword);
		client.setPhone(clientPhone);
		client.setPoints(clientPoints);
		client.setUsername(clientUsername);
		client.setValidationCode(clientValidationCode);
		boolean verify = clientBusiness.createClient(client);
		if (verify) {
			FacesMessages.info("Votre compte " + clientUsername + " a bien été créé :) ");
		} else {
			FacesMessages.warning("Oups c'est déjà pris ou les champs ne sont pas respectés :O");
			forward = null;
		}
		return forward;
	}



	// *** CONNECT ***

	public String connectUser() {
		String forward = null;
		
		if (username.length()>1) {
			Admin myAdmin = new Admin();
			myAdmin.setUsername(username);
			myAdmin.setPassword(password);
			admin = adminBusiness.connectAdmin(myAdmin);

			Partner myPartner = new Partner();
			myPartner.setUsername(username);
			myPartner.setPassword(password);
			partner = partnerBusiness.connectPartner(myPartner);

			Client myClient = new Client();
			myClient.setUsername(username);
			myClient.setPassword(password);
			client = clientBusiness.connectClient(myClient);

			if (isAdminConnected()) {
				loggedUser = " : " + admin.getUsername() + " [Admin]";
				FacesMessages.info("Bonjour " + admin.getUsername() + " :) ");
				forward = "/admin-operations.xhtml?faces-redirection=false";
			} else if (isPartnerConnected()) {
				loggedUser = " : " + partner.getUsername() + " [Partner]";
				FacesMessages.info("Bonjour " + partner.getUsername() + " :) ");
				forward = "/home.xhtml?faces-redirection=true";
			} else if (isClientConnected()) {
				loggedUser = " : " + client.getUsername();
				FacesMessages.info("Bonjour " + client.getUsername() + " :) ");
				forward = "/home.xhtml?faces-redirection=true";
			} else {
				FacesMessages.warning("Votre identifiant ou mot de passe est incorrect :O");
				forward = "/login.xhtml?faces-redirection=false";
			}
		} else {
			FacesMessages.info("Veuillez rentrer votre identifiant et mot de passe");
		}
		
		return forward;
	}

	public boolean isUserConnected() {
		return (isAdminConnected()||isPartnerConnected()||isClientConnected());
	}

	public boolean isAdminConnected() {
		boolean verify = false;
		if (admin!=null) {
			verify = true;
		}
		return verify;
	}

	public boolean isPartnerConnected() {
		boolean verify = false;
		if (partner!=null) {
			verify = true;
		}
		return verify;
	}

	public boolean isClientConnected() {
		boolean verify = false;
		if (client!=null) {
			verify = true;
		}
		return verify;
	}

	// *** DISCONNECT ***

	public String disconnect() {
		if (isUserConnected()) FacesMessages.info("A bientôt ! ");
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		admin = null;
		partner = null;
		client = null;
		loggedUser = null;
		clearFields();
		return "/login.xhtml?faces-redirection=true";
	}



	// ***** GETTERS & SETTERS *****

	public LocalDate getPartnerBirthdate() {
		return partnerBirthdate;
	}

	public void setPartnerBirthdate(LocalDate partnerBirthdate) {
		this.partnerBirthdate = partnerBirthdate;
	}

	public String getPartnerDescription() {
		return partnerDescription;
	}

	public void setPartnerDescription(String partnerDescription) {
		this.partnerDescription = partnerDescription;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerFirstName() {
		return partnerFirstName;
	}

	public void setPartnerFirstName(String partnerFirstName) {
		this.partnerFirstName = partnerFirstName;
	}

	public String getPartnerLastName() {
		return partnerLastName;
	}

	public void setPartnerLastName(String partnerLastName) {
		this.partnerLastName = partnerLastName;
	}

	public String getPartnerOrganization() {
		return partnerOrganization;
	}

	public void setPartnerOrganization(String partnerOrganization) {
		this.partnerOrganization = partnerOrganization;
	}

	public String getPartnerPassword() {
		return partnerPassword;
	}

	public void setPartnerPassword(String partnerPassword) {
		this.partnerPassword = partnerPassword;
	}

	public String getPartnerPhone() {
		return partnerPhone;
	}

	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}

	public String getPartnerPicture() {
		return partnerPicture;
	}

	public void setPartnerPicture(String partnerPicture) {
		this.partnerPicture = partnerPicture;
	}

	public String getPartnerTitle() {
		return partnerTitle;
	}

	public void setPartnerTitle(String partnerTitle) {
		this.partnerTitle = partnerTitle;
	}

	public String getPartnerUsername() {
		return partnerUsername;
	}

	public void setPartnerUsername(String partnerUsername) {
		this.partnerUsername = partnerUsername;
	}

	public String getPartnerValidationCode() {
		return partnerValidationCode;
	}

	public void setPartnerValidationCode(String partnerValidationCode) {
		this.partnerValidationCode = partnerValidationCode;
	}

	public Integer getPartnerCityFK() {
		return partnerCityFK;
	}

	public void setPartnerCityFK(Integer partnerCityFK) {
		this.partnerCityFK = partnerCityFK;
	}

	public LocalDate getClientBirthdate() {
		return clientBirthdate;
	}

	public void setClientBirthdate(LocalDate clientBirthdate) {
		this.clientBirthdate = clientBirthdate;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientFirstName() {
		return clientFirstName;
	}

	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}

	public String getClientLastName() {
		return clientLastName;
	}

	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}

	public String getClientPassword() {
		return clientPassword;
	}

	public void setClientPassword(String clientPassword) {
		this.clientPassword = clientPassword;
	}

	public String getClientPhone() {
		return clientPhone;
	}

	public void setClientPhone(String clientPhone) {
		this.clientPhone = clientPhone;
	}

	public Integer getClientPoints() {
		return clientPoints;
	}

	public void setClientPoints(Integer clientPoints) {
		this.clientPoints = clientPoints;
	}

	public String getClientUsername() {
		return clientUsername;
	}

	public void setClientUsername(String clientUsername) {
		this.clientUsername = clientUsername;
	}

	public String getClientValidationCode() {
		return clientValidationCode;
	}

	public void setClientValidationCode(String clientValidationCode) {
		this.clientValidationCode = clientValidationCode;
	}

	public Integer getClientCityFK() {
		return clientCityFK;
	}

	public void setClientCityFK(Integer clientCityFK) {
		this.clientCityFK = clientCityFK;
	}

	public String getDateConvert() {
		return dateConvert;
	}

	public void setDateConvert(String dateConvert) {
		this.dateConvert = dateConvert;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(String loggedUser) {
		this.loggedUser = loggedUser;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public String getClientMessage() {
		return clientMessage;
	}

	public void setClientMessage(String clientMessage) {
		this.clientMessage = clientMessage;
	}

	public String getPartnerMessage() {
		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {
		this.partnerMessage = partnerMessage;
	}	
}
