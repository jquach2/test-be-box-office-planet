#!/bin/bash
if [ `minikube status | grep Running | wc -l` == 0 ]
then 
    minikube start
else
    echo minikube status
fi
kubectl apply -f 1-namespace.yaml
kubectl apply -f 2-volume.yaml
kubectl apply -f 3-mysql.yaml
while [ `kubectl rollout status deploy mysql-deployment -n production | grep success | wc -l` == 0 ]
do
	sleep 1
done
kubectl apply -f 4-app.yaml
kubectl get namespaces
minikube tunnel
