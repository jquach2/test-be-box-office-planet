package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.RewardType;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface RewardTypeIBusiness {
	
	//CREATE
	/** creates single rewardType */
	boolean createRewardType(RewardType rewardType);
	
	//READ
	/** returns list of rewardTypes */
	List<RewardType> findAllRewardTypes();
	
	/** returns list of rewardTypes filtering by name */
	List<RewardType> findRewardTypesByName(String name);
	
	/** returns single rewardType using id */
	RewardType findRewardTypeById(Integer id);
	
	/** returns number of rewardTypes */
	Long findNbRewardTypes();
	
	//UPDATE
	/** updates single rewardType **/
	boolean updateRewardType(RewardType rewardType);
	
	//DELETE
	/** removes single rewardType **/
	boolean deleteRewardType(RewardType rewardType);
}
