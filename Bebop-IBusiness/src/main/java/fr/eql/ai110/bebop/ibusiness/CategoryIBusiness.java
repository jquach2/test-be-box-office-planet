package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Category;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface CategoryIBusiness {
	
	//CREATE
	/** creates single category */
	boolean createCategory(Category category);
	
	//READ
	/** returns list of categories */
	List<Category> findAllCategories();
	
	/** returns list of categories filtering by name */
	List<Category> findCategoriesByName(String name);
	
	/** returns single category using id */
	Category findCategoryById(Integer id);
	
	/** returns number of categories */
	Long findNbCategories();
	
	//UPDATE
	/** updates single category **/
	boolean updateCategory(Category category);
	
	//DELETE
	/** removes single category **/
	boolean deleteCategory(Category category);
}
