package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Space;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface SpaceIBusiness {
	
	//CREATE
	/** creates single space */
	boolean createSpace(Space space);
	
	//READ
	/** returns list of spaces */
	List<Space> findAllSpaces();
	
	/** returns list of spaces filtering by name */
	List<Space> findSpacesByName(String name);
	
	/** returns single space using id */
	Space findSpaceById(Integer id);
	
	/** returns number of spaces */
	Long findNbSpaces();
	
	//UPDATE
	/** updates single space **/
	boolean updateSpace(Space space);
	
	//DELETE
	/** removes single space **/
	boolean deleteSpace(Space space);
}
