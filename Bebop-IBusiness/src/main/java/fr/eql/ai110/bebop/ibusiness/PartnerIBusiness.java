package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Partner;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface PartnerIBusiness {
	
	//AUTHENTICATE
	/** hashes password **/
	String hashPassword(String password);
	
	/** connects partner **/
	Partner connectPartner(Partner partner);
	
	//CREATE
	/** creates single partner */
	boolean createPartner(Partner partner);
	
	//READ
	/** returns list of partners */
	List<Partner> findAllPartners();
	
	/** returns list of partners by multi */
	List<Partner> findPartnersByMulti(Partner partner);
	
	/** returns list of partners filtering by name */
	List<Partner> findPartnersByName(String name);
	
	/** returns single partner using id */
	Partner findPartnerById(Integer id);
	
	/** returns number of partners */
	Long findNbPartners();
	
	//UPDATE
	/** updates single partner **/
	boolean updatePartner(Partner partner);
	
	//DELETE
	/** removes single partner **/
	boolean deletePartner(Partner partner);
}
