package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Wish;


/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface WishIBusiness {
	
	//CREATE
	/** creates single wish */
	boolean createWish(Wish wish);
	
	//READ
	/** returns list of wishes */
	List<Wish> findAllWishes();
	
	/** returns list of wishes filtering by name */
	List<Wish> findWishesByName(String name);
	
	/** returns single wish using id */
	Wish findWishById(Integer id);
	
	/** returns number of wishes */
	Long findNbWishes();
	
	//UPDATE
	/** updates single wish **/
	boolean updateWish(Wish wish);
	
	//DELETE
	/** removes single wish **/
	boolean deleteWish(Wish wish);
}
