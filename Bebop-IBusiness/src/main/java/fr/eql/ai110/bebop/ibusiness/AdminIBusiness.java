package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Admin;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface AdminIBusiness {
	
	//AUTHENTICATION
	/** hash password **/
	String hashPassword(String password);
	
	/** authenticate admin **/
	Admin connectAdmin(Admin admin);
	
	//CREATE
	/** creates single admin */
	boolean createAdmin(Admin admin);
	
	//READ
	/** returns list of admins */
	List<Admin> findAllAdmins();
	
	/** returns list of admins filtering by name */
	List<Admin> findAdminsByName(String name);
	
	/** returns list of admins by multicriteria */
	List<Admin> findAdminsByMulti(Admin admin);
	
	/** returns single admin using id */
	Admin findAdminById(Integer id);
	
	/** returns number of admins */
	Long findNbAdmins();
	
	//UPDATE
	/** updates single admin **/
	boolean updateAdmin(Admin admin);
	
	//DELETE
	/** removes single admin **/
	boolean deleteAdmin(Admin admin);

	
}
