package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Subset;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface SubsetIBusiness {
	
	//CREATE
	/** creates single subset */
	boolean createSubset(Subset subset);
	
	//READ
	/** returns list of subset */
	List<Subset> findAllSubsets();
	
	/** returns list of archetype entities filtering by name */
	List<Subset> findSubsetsByName(String name);
	
	/** returns single subset using id */
	Subset findSubsetById(Integer id);
	
	/** returns number of subsets */
	Long findNbSubsets();
	
	//UPDATE
	/** updates single subset **/
	boolean updateSubset(Subset subset);
	
	//DELETE
	/** removes single subset **/
	boolean deleteSubset(Subset subset);
}
