package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.Day;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface DayIBusiness {
	
	//CREATE
	/** creates single day */
	boolean createDay(Day day);
	
	//READ
	/** returns list of days */
	List<Day> findAllDays();
	
	/** returns list of days filtering by name */
	List<Day> findDaysByName(String name);
	
	/** returns single day using id */
	Day findDayById(Integer id);
	
	/** returns number of days */
	Long findNbDays();
	
	//UPDATE
	/** updates single day **/
	boolean updateDay(Day day);
	
	//DELETE
	/** removes single day **/
	boolean deleteDay(Day day);
}
