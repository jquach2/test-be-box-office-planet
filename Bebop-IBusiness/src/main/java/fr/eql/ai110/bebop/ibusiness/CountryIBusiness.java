package fr.eql.ai110.bebop.ibusiness;

import java.util.List;

import fr.eql.ai110.bebop.entity.ArchetypeEntity;
import fr.eql.ai110.bebop.entity.Country;

/**
 * 
 * Adds control layer to be implemented at Business level
 * before executing Dao operations
 * @author Thibault
 *
 */
public interface CountryIBusiness {
	
	//CREATE
	/** creates single country */
	boolean createCountry(Country country);
	
	//READ
	/** returns list of country */
	List<Country> findAllCountries();
	
	/** returns list of countries filtering by name */
	List<Country> findCountriesByName(String name);
	
	/** returns single country using id */
	Country findCountryById(Integer id);
	
	/** returns number of country */
	Long findNbCountries();
	
	//UPDATE
	/** updates single country **/
	boolean updateCountry(Country country);
	
	//DELETE
	/** removes single country **/
	boolean deleteCountry(Country country);
}
