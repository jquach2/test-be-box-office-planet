INSERT INTO `admin` VALUES (1,'2022-02-20 12:12:27',NULL,'thibault.charrin@gmail.com','Thibault','Charrin','��p��U��F�H�v(b�4��v@d�鷏��Z�',NULL,'thib7861'),(2,'2022-02-20 12:14:54',NULL,'axel@gmail.com','Axel','Cats','�G�X���E���n<#�q�n�-��p��\"��','2022-02-20 12:14:54','axel2021'),(3,'2022-02-20 22:07:13',NULL,'alexis@gmail.com','Alexis','Arrial','�S�FI�]�X�BC�W��{̉�9r|:�h|�jcs',NULL,'potrunk'),(4,'2022-02-20 23:07:00',NULL,'sylvie@gmail.com','Sylvie','Ma','T�1��¯r� ��\"�-���\Z&�6�0�!',NULL,'vivi'),(5,'2022-02-20 23:13:12',NULL,'jar@gmail.com','Jean-Antoine','Maro','c�CP$�#M@��%�!qN#E_��',NULL,'jar'),(6,'2022-02-20 23:14:50',NULL,'mulora@gmail.com','Laura','Murat','5��p��H^5�Y�@���*��o6���h�',NULL,'mulora');

INSERT INTO `archetype_entity` VALUES (1,'Test');

INSERT INTO `day` VALUES (1,'Lundi',NULL),(2,'Mardi',NULL),(3,'Mercredi',NULL),(4,'Jeudi',NULL),(5,'Vendredi',NULL),(6,'Samedi',NULL),(7,'Dimanche',NULL);

INSERT INTO `category` VALUES (1,'Concert'),(2,'Théâtre');

INSERT INTO `subset` VALUES (1,'Classique',1),(2,'Jazz',1),(3,'Comédie',2),(4,'Drame',2);

INSERT INTO `country` VALUES (1,'France'),(2,'Royaume-Uni'),(3,'Allemagne'),(4,'Italie'),(5,'Suisse'),(6,'Etats-Unis');

INSERT INTO `city` VALUES (1,'Paris',1),(2,'Londres',2),(3,'Berlin',3),(4,'Rome',4),(5,'Lucerne',5),(6,'New York',6),(7,'Los Angeles',6);

INSERT INTO `venue` VALUES (1,'221 Av. Jean Jaurès',NULL,'https://philharmoniedeparis.fr/fr','Philharmonie de Paris','philharmonie.jpg',1);

INSERT INTO `client` VALUES (1,'1987-03-18','2022-02-22 23:27:00',NULL,'baptiste@gmail.com','Baptiste','Mouchard','�h,NY�\\�������N�ƑT\Za��B��S',NULL,'0605040302',2600,'baptcode',NULL,NULL,1),(2,'1992-12-19','2022-02-23 19:31:39','2022-02-23 23:46:44','nico@gmail.com','Nicholas','Chopin','���lk���`Q��xZl���d��G���|..\"q',NULL,'0675491824',NULL,'nico',NULL,NULL,1),(3,'1999-01-01','2022-02-23 20:34:26',NULL,'lara@gmail.com','Lara','Croft','+*�����}XQ*�\r{@�ڻ��M�x�(�p',NULL,'0605040080',NULL,'lara',NULL,NULL,2),(4,'1956-07-08','2022-02-23 20:46:44','2022-02-23 22:46:44','paul@gmail.com','Paul','Newman','WQ=�:nt��u$������ԣX�G�HA\0','2022-02-23 22:46:44','0105040708',NULL,'paul',NULL,NULL,5);

INSERT INTO `partner` VALUES (1,NULL,'1994-09-26','2022-02-22 23:29:37',NULL,'Chargé de la programmation musicale de la salle Pierre Boulez','eric@gmail.com','Eric','Guesnet','Philharmonie de Paris','�l�a(�B�|�6�4T=����_>�9+[�D�',NULL,'0607080904',NULL,'Directeur','efguesnet',NULL,NULL,1),(2,'2022-02-23 19:35:22','1966-04-05','2022-02-23 19:34:30',NULL,'Chargé de la programmation musicale','arty@gmail.com','Arty','Mozart','Salle Cortot','|��\'���7�\ZH�:�f���~�䑖pRKf��',NULL,'0774518956',NULL,'Directeur Artistique','arty',NULL,NULL,1),(3,NULL,'1959-06-05','2022-02-23 20:36:20','2022-02-23 23:36:20','','chris@gmail.com','Christopher','Axworthy','Wigmore Hall','̆��P�?l��>R��x�3��1��y��P�;�;',NULL,'0778594806',NULL,'Music Director','pianomag',NULL,NULL,2),(4,NULL,'1966-05-17','2022-02-23 20:41:21',NULL,'World Class orchestra, season 2022-2023 available','gustavo@gmail.com','Gustavo','Dudamel','LA Philharmonic','gڮ��a(W� /F3V��\Z��@�JK���\'Nm',NULL,'0849575654',NULL,'Artistic Director','gustavo',NULL,NULL,7),(5,'2022-02-23 23:38:20','1970-07-04','2022-02-23 20:44:04','2022-02-23 23:38:20','Theatre / Film and Television actor','strange@gmail.com','Benedict','Cumberbatch','MCU','A��o�gCR� �G�3f���A\"�(�&\nh��',NULL,'0909090909',NULL,'Actor','strange',NULL,NULL,6);

INSERT INTO `reward_type` VALUES (1,'','Spotify Premium 10€','spotify.jpg',300),(2,'','Spotify Premium 20€','spotify.jpg',600),(3,'','Spotify Premium 30€','spotify.jpg',800),(4,'','Spotify Premium 50€','spotify.jpg',1000),(5,'','Netflix 10€','netflix.jpg',300),(6,'','Netflix 30€','netflix.jpg',800),(7,'','Netflix 60€','netflix.jpg',1200),(8,'','App Store & iTunes 50€','apple.jpg',1000),(9,'','App Store & iTunes 100€','apple.jpg',2000);

INSERT INTO `reward` VALUES (1,'SPOT10',NULL,'Q4F6R8G4S5R8G',NULL,1),(2,'SPOT10',NULL,'S6D5F4E8Z9G5F',NULL,1),(3,'SPOT10',NULL,'Q7S9D8F4Z5D5G',NULL,1),(4,'SPOT20',NULL,'S7D9G8R7S5D6G',NULL,2),(5,'SPOT20',NULL,'S7D9F8G7D8F9HG',NULL,2),(6,'SPOT20',NULL,'S7D9F8D7H8F9S',NULL,2),(7,'SPOT30',NULL,'S7D8H9F8SD7F8HG',NULL,3),(8,'SPOT30',NULL,'S79H8F7S8F',NULL,3),(9,'SPOT50',NULL,'S7DF8G9S8F7G',NULL,4),(10,'SPOT50',NULL,'SDF9S8D7F987',NULL,4),(11,'NETF10',NULL,'AZE98D7F94',NULL,5),(12,'NETF10',NULL,'SDF7E8RH97',NULL,5),(13,'NETF10',NULL,'DF9G87SD987G',NULL,5),(14,'NETF30',NULL,'DSG98E7R',NULL,6),(15,'NETF60',NULL,'AZ9R8798D',NULL,7),(16,'NETF60',NULL,'SDFPOIZ808',NULL,7),(17,'APPL50',NULL,'SD9F8798Z',NULL,8),(18,'APPL50',NULL,'AZR987DFH',NULL,8),(19,'APPL50',NULL,'DSFG80Z80H',NULL,8),(20,'APPL100',NULL,'S9G87ER9TH87',NULL,9),(21,'APPL100',NULL,'DFH789R8H7987',NULL,9),(22,'APPL100',NULL,'AZE78F7GH',NULL,9);

INSERT INTO `space` VALUES (1,2400,'Salle Pierre Boulez',1);

