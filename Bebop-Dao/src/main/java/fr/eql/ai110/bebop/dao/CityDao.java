package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.City;
import fr.eql.ai110.bebop.idao.CityIDao;


@Remote(CityIDao.class)
@Stateless
public class CityDao extends GenericDao<City> implements CityIDao {

	@Override
	public Long countCities() {
		Query query = em.createQuery("SELECT COUNT(c) FROM City c");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<City> c;
		Query query = em.createQuery("SELECT c FROM City c WHERE c.name= :nameParam");
		query.setParameter("nameParam", name);
		c = query.getResultList();
		if (c.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<City> getCitiesByName(String name) {
		Query query = em.createQuery("SELECT c FROM City c WHERE c.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<City>)query.getResultList();
	}
}
