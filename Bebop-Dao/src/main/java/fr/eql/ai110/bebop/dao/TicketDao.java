package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Ticket;
import fr.eql.ai110.bebop.idao.TicketIDao;



@Remote(TicketIDao.class)
@Stateless
public class TicketDao extends GenericDao<Ticket> implements TicketIDao {

	@Override
	public Long countTickets() {
		Query query = em.createQuery("SELECT COUNT(t) FROM Ticket t");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Ticket> t;
		Query query = em.createQuery("SELECT t FROM Ticket t WHERE t.name= :nameParam");
		query.setParameter("nameParam", name);
		t = query.getResultList();
		if (t.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Ticket> getTicketsByName(String name) {
		Query query = em.createQuery("SELECT t FROM Ticket t WHERE t.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Ticket>)query.getResultList();
	}
}
