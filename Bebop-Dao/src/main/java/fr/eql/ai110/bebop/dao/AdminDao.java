package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Admin;
import fr.eql.ai110.bebop.idao.AdminIDao;

@Remote(AdminIDao.class)
@Stateless
public class AdminDao extends GenericDao<Admin> implements AdminIDao {

	@Override
	public Long countAdmins() {
		Query query = em.createQuery("SELECT COUNT(a) FROM Admin a");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Admin> a;
		Query query = em.createQuery("SELECT a FROM Admin a WHERE a.username= :nameParam");
		query.setParameter("nameParam", name);
		a = query.getResultList();
		if (a.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Admin> getAdminsByName(String name) {
		Query query = em.createQuery("SELECT a FROM Admin a WHERE a.username LIKE :nameParam ORDER BY a.username ASC");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Admin>)query.getResultList();
	}

	@Override
	public List<Admin> getAdminsByMulti(String username,
			String email, String firstname, String lastname) {
		Query query = em.createQuery("SELECT a FROM Admin a "
				+ "WHERE a.username LIKE :usernameParam "
				+ "AND a.emailRecovery LIKE :emailParam "
				+ "AND a.firstName LIKE :firstNameParam "
				+ "AND a.lastName LIKE :lastNameParam "
				+ "ORDER BY a.username ASC");
		query.setParameter("emailParam", '%' + username + '%');
		query.setParameter("firstNameParam", '%' + email + '%');
		query.setParameter("lastNameParam", '%' + firstname + '%');
		query.setParameter("usernameParam", '%' + lastname + '%');
		return (List<Admin>)query.getResultList();
	}
	
	@Override
	public Admin adminAuthenticate(String username, String password) {
		Admin admin = null;
		List<Admin> admins;
		Query query = em.createQuery("SELECT a FROM Admin a WHERE a.username= :usernameParam "
				+ "AND a.password= :passwordParam");
		query.setParameter("usernameParam", username);
		query.setParameter("passwordParam", password);
		admins = query.getResultList();
		if (admins.size() > 0) {
			admin = admins.get(0);
		}
		return admin;
	}
}
