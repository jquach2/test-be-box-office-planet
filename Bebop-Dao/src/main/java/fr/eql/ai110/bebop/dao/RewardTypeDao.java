package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.RewardType;
import fr.eql.ai110.bebop.idao.RewardTypeIDao;


@Remote(RewardTypeIDao.class)
@Stateless
public class RewardTypeDao extends GenericDao<RewardType> implements RewardTypeIDao {

	@Override
	public Long countRewardTypes() {
		Query query = em.createQuery("SELECT COUNT(r) FROM Rewards r");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<RewardType> rt;
		Query query = em.createQuery("SELECT rt FROM RewardType rt WHERE rt.name= :nameParam");
		query.setParameter("nameParam", name);
		rt = query.getResultList();
		if (rt.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<RewardType> getRewardTypesByName(String name) {
		Query query = em.createQuery("SELECT rt FROM RewardType rt WHERE rt.name LIKE :nameParam ORDER BY rt.name ASC");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<RewardType>)query.getResultList();
	}
}
