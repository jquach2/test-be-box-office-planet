package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Client;
import fr.eql.ai110.bebop.entity.Partner;
import fr.eql.ai110.bebop.idao.PartnerIDao;


@Remote(PartnerIDao.class)
@Stateless
public class PartnerDao extends GenericDao<Partner> implements PartnerIDao {

	@Override
	public Long countPartners() {
		Query query = em.createQuery("SELECT COUNT(p) FROM Partner p");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Partner> p;
		Query query = em.createQuery("SELECT p FROM Partner p WHERE p.username= :nameParam");
		query.setParameter("nameParam", name);
		p = query.getResultList();
		if (p.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Partner> getPartnersByName(String name) {
		Query query = em.createQuery("SELECT p FROM Partner p WHERE p.username LIKE :nameParam ORDER BY p.username ASC");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Partner>)query.getResultList();
	}

	@Override
	public List<Partner> getPartnersByMulti(String username, String email, String firstName, String lastName) {
		Query query = em.createQuery("SELECT p FROM Partner p "
				+ "WHERE p.username LIKE :usernameParam "
				+ "AND p.email LIKE :emailParam "
				+ "AND p.firstName LIKE :firstNameParam "
				+ "AND p.lastName LIKE :lastNameParam "
				+ "ORDER BY p.username ASC");
		query.setParameter("emailParam", '%' + username + '%');
		query.setParameter("firstNameParam", '%' + email + '%');
		query.setParameter("lastNameParam", '%' + firstName + '%');
		query.setParameter("usernameParam", '%' + lastName + '%');
		return (List<Partner>)query.getResultList();
	}

	@Override
	public Partner partnerAuthenticate(String username, String password) {
		Partner partner = null;
		List<Partner> partners;
		Query query = em.createQuery("SELECT p FROM Partner p WHERE p.username= :usernameParam "
				+ "AND p.password= :passwordParam");
		query.setParameter("usernameParam", username);
		query.setParameter("passwordParam", password);
		partners = query.getResultList();
		if (partners.size() > 0) {
			partner = partners.get(0);
		}
		return partner;
	}
}
