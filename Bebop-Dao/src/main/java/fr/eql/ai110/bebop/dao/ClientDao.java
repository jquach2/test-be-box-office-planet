package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Client;
import fr.eql.ai110.bebop.idao.ClientIDao;


@Remote(ClientIDao.class)
@Stateless
public class ClientDao extends GenericDao<Client> implements ClientIDao {

	@Override
	public Long countClients() {
		Query query = em.createQuery("SELECT COUNT(c) FROM Client c");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Client> c;
		Query query = em.createQuery("SELECT c FROM Client c WHERE c.username= :nameParam");
		query.setParameter("nameParam", name);
		c = query.getResultList();
		if (c.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Client> getClientsByName(String name) {
		Query query = em.createQuery("SELECT c FROM Client c WHERE c.username LIKE :nameParam ORDER BY c.username ASC");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Client>)query.getResultList();
	}

	@Override
	public List<Client> getClientsByMulti(String username, String email, String firstName, String lastName) {
		Query query = em.createQuery("SELECT c FROM Client c "
				+ "WHERE c.username LIKE :usernameParam "
				+ "AND c.email LIKE :emailParam "
				+ "AND c.firstName LIKE :firstNameParam "
				+ "AND c.lastName LIKE :lastNameParam "
				+ "ORDER BY c.username ASC");
		query.setParameter("emailParam", '%' + username + '%');
		query.setParameter("firstNameParam", '%' + email + '%');
		query.setParameter("lastNameParam", '%' + firstName + '%');
		query.setParameter("usernameParam", '%' + lastName + '%');
		return (List<Client>)query.getResultList();
	}

	@Override
	public Client clientAuthenticate(String username, String password) {
		Client client = null;
		List<Client> clients;
		Query query = em.createQuery("SELECT c FROM Client c WHERE c.username= :usernameParam "
				+ "AND c.password= :passwordParam");
		query.setParameter("usernameParam", username);
		query.setParameter("passwordParam", password);
		clients = query.getResultList();
		if (clients.size() > 0) {
			client = clients.get(0);
		}
		return client;
	}
}
