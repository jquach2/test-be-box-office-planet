package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Wish;
import fr.eql.ai110.bebop.idao.WishIDao;


@Remote(WishIDao.class)
@Stateless
public class WishDao extends GenericDao<Wish> implements WishIDao {

	@Override
	public Long countWishes() {
		Query query = em.createQuery("SELECT COUNT(w) FROM Wish w");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Wish> w;
		Query query = em.createQuery("SELECT w FROM Wish w WHERE w.name= :nameParam");
		query.setParameter("nameParam", name);
		w = query.getResultList();
		if (w.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Wish> getWishesByName(String name) {
		Query query = em.createQuery("SELECT w FROM Wish w WHERE w.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Wish>)query.getResultList();
	}
}
