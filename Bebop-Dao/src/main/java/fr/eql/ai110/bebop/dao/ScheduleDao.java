package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Schedule;
import fr.eql.ai110.bebop.idao.ScheduleIDao;


@Remote(ScheduleIDao.class)
@Stateless
public class ScheduleDao extends GenericDao<Schedule> implements ScheduleIDao {

	@Override
	public Long countSchedules() {
		Query query = em.createQuery("SELECT COUNT(s) FROM Schedule s");
		return (Long) query.getSingleResult();
	}
}
