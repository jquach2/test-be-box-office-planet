package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Event;
import fr.eql.ai110.bebop.idao.EventIDao;


@Remote(EventIDao.class)
@Stateless
public class EventDao extends GenericDao<Event> implements EventIDao {

	@Override
	public Long countEvents() {
		Query query = em.createQuery("SELECT COUNT(e) FROM Event e");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Event> e;
		Query query = em.createQuery("SELECT e FROM Event e WHERE e.name= :nameParam");
		query.setParameter("nameParam", name);
		e = query.getResultList();
		if (e.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Event> getEventsByName(String name) {
		Query query = em.createQuery("SELECT e FROM Event e WHERE e.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Event>)query.getResultList();
	}
}
