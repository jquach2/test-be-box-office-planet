package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Space;
import fr.eql.ai110.bebop.idao.SpaceIDao;


@Remote(SpaceIDao.class)
@Stateless
public class SpaceDao extends GenericDao<Space> implements SpaceIDao {

	@Override
	public Long countSpaces() {
		Query query = em.createQuery("SELECT COUNT(s) FROM Space s");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Space> h;
		Query query = em.createQuery("SELECT h FROM Hall h WHERE h.name= :nameParam");
		query.setParameter("nameParam", name);
		h = query.getResultList();
		if (h.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Space> getSpacesByName(String name) {
		Query query = em.createQuery("SELECT s FROM Space s WHERE s.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Space>)query.getResultList();
	}
}
