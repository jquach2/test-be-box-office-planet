package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Venue;
import fr.eql.ai110.bebop.idao.VenueIDao;



@Remote(VenueIDao.class)
@Stateless
public class VenueDao extends GenericDao<Venue> implements VenueIDao {

	@Override
	public Long countVenues() {
		Query query = em.createQuery("SELECT COUNT(v) FROM Venue v");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Venue> v;
		Query query = em.createQuery("SELECT v FROM Venue v WHERE v.name= :nameParam");
		query.setParameter("nameParam", name);
		v = query.getResultList();
		if (v.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Venue> getVenuesByName(String name) {
		Query query = em.createQuery("SELECT v FROM Venue v WHERE v.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Venue>)query.getResultList();
	}
}
