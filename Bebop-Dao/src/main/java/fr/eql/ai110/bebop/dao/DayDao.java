package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Day;
import fr.eql.ai110.bebop.idao.DayIDao;


@Remote(DayIDao.class)
@Stateless
public class DayDao extends GenericDao<Day> implements DayIDao {

	@Override
	public Long countDays() {
		Query query = em.createQuery("SELECT COUNT(d) FROM Days d");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Day> d;
		Query query = em.createQuery("SELECT d FROM Day d WHERE d.name= :nameParam");
		query.setParameter("nameParam", name);
		d = query.getResultList();
		if (d.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Day> getDaysByName(String name) {
		Query query = em.createQuery("SELECT d FROM Day d WHERE d.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Day>)query.getResultList();
	}
}
