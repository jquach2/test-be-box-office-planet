package fr.eql.ai110.bebop.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.bebop.entity.Subset;
import fr.eql.ai110.bebop.idao.SubsetIDao;


@Remote(SubsetIDao.class)
@Stateless
public class SubsetDao extends GenericDao<Subset> implements SubsetIDao {

	@Override
	public Long countSubsets() {
		Query query = em.createQuery("SELECT COUNT(s) FROM Subset s");
		return (Long) query.getSingleResult();
	}

	@Override
	public boolean isNameAvailable(String name) {
		boolean result = true;
		List<Subset> s;
		Query query = em.createQuery("SELECT s FROM Subset s WHERE s.name= :nameParam");
		query.setParameter("nameParam", name);
		s = query.getResultList();
		if (s.size() > 0) {
			result = false;
		}
		return result;
	}

	@Override
	public List<Subset> getSubsetsByName(String name) {
		Query query = em.createQuery("SELECT s FROM Subset s WHERE s.name LIKE :nameParam ");
		query.setParameter("nameParam", '%' + name + '%');
		return (List<Subset>)query.getResultList();
	}
}
